<?php 
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new Animal("Shaun");
echo "Nama hewan : $sheep->name <br>";
echo "Jumlah kaki : $sheep->legs <br>";
echo "Hewan bertipe : $sheep->coldBlooded <br><br>";

$sungokong = new Ape("Kera Sakti");
echo "$sungokong->name bisa teriak "; 
$sungokong->yell(); // "Auooo"
echo "<br>";

$kodok = new Frog("Buduk");
echo "$kodok->name bisa melompat "; 
$kodok->jump(); // "hop hop"


?>